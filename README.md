# LiteX RISC-V playground

## Examples

```ShellSession
# python3 -m litex_boards.targets.lattice_ice40up5k_evn --cpu-variant minimal --build
```

```ShellSession
# litex_sim --cpu-variant minimal
```

## Known issues

* `Too many open files` in macOS
   * https://github.com/docker/for-mac/issues/2354

## License

The files in this repository are licensed under the [CC0 1.0 Universal license](https://creativecommons.org/publicdomain/zero/1.0/legalcode).
