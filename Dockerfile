FROM ubuntu:22.04

RUN apt-get -y update && \
  apt-get -y upgrade && \
  apt-get -y install \
    curl \
    device-tree-compiler \
    g++ \
    gcc \
    git \
    gnupg \
    libevent-dev \
    libjson-c-dev \
    make \
    python3 \
    zlib1g-dev && \
  echo "deb [signed-by=/etc/apt/keyrings/sbt.gpg] https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list && \
  echo "deb [signed-by=/etc/apt/keyrings/sbt.gpg] https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list && \
  curl -Ls "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | gpg --dearmor -o /etc/apt/keyrings/sbt.gpg && \
  apt-get -y update && \
  apt-get -y install \
    openjdk-17-jdk-headless \
    sbt && \
  apt-get -y autoremove && \
  apt-get -y autoclean && \
  rm -fR /var/lib/apt/lists/*
RUN mkdir -p /opt/litex/share
RUN curl -o ${HOME}/get-pip.py "https://bootstrap.pypa.io/get-pip.py" && \
  python3 ${HOME}/get-pip.py && \
  rm ${HOME}/get-pip.py
RUN python3 -m pip install --user \
  meson \
  ninja

WORKDIR /opt/litex/share

RUN curl -L -o ${HOME}/litex_setup.py "https://raw.githubusercontent.com/enjoy-digital/litex/master/litex_setup.py" && \
  python3 ${HOME}/litex_setup.py --init --install --user --config=full

WORKDIR ${HOME}

RUN curl -L -o ${HOME}/oss-cad-suite.tgz "https://github.com/YosysHQ/oss-cad-suite-build/releases/download/2022-09-25/oss-cad-suite-linux-x64-20220925.tgz" && \
  tar -xz -f ${HOME}/oss-cad-suite.tgz -C /opt && \
  rm ${HOME}/oss-cad-suite.tgz
RUN curl -L -o ${HOME}/xpack-riscv-none-elf-gcc.tar.gz "https://github.com/xpack-dev-tools/riscv-none-elf-gcc-xpack/releases/download/v12.2.0-1/xpack-riscv-none-elf-gcc-12.2.0-1-linux-x64.tar.gz" && \
  tar -xz -f ${HOME}/xpack-riscv-none-elf-gcc.tar.gz --one-top-level=/opt/xpack-riscv-none-elf-gcc --strip-components=1 && \
  rm ${HOME}/xpack-riscv-none-elf-gcc.tar.gz
RUN echo "PATH=\"\${HOME}/.local/bin:\${PATH}\"" >> ${HOME}/.profile && \
  echo "PATH=\"/opt/xpack-riscv-none-elf-gcc/bin:\${PATH}\"" >> ${HOME}/.profile && \
  echo "PATH=\"/opt/oss-cad-suite/bin:\${PATH}\"" >> ${HOME}/.profile && \
  echo "export PATH" >> ${HOME}/.profile

CMD ["/bin/bash", "-l"]
